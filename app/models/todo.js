const mongoose = require('mongoose')

const Schema = mongoose.Schema

const TodoSchema = new Schema({
  libelle: {
    type: String,
    required: true
  },
  done: {
    type: Boolean,
    default:false
  },
  createdAt: {
    type: Date,
    default: Date.now
  }, 
  updatedAt: {
    type: Date,
    default: Date.now
  }
})


module.exports = mongoose.model('Todo', TodoSchema)

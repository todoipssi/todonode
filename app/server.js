const express = require('express'),
  bodyParser = require('body-parser'),
  mongoose = require('mongoose'),
  morgan = require('morgan'),
  config = require('config'),
  app = express()

// API Services
const serviceGet = require('./services/get'),
      servicePost=require('./services/post'),
      serviceUpdate=require('./services/update'),
      serviceDelete=require('./services/delete');
     

const API_CONFIG = config.api
// set our port
const PORT = process.env.PORT || API_CONFIG.port

// create our router
const router = express.Router()

const db = mongoose.connection

const HOST = `${config.db.host}${config.db.port}`
mongoose.connect(HOST)

db.on('error', console.error.bind(console, 'connection error:'))
db.once('open', () => console.log('DB Connected'))



app.use(morgan('dev'))


app.use((req,res,next)=>{
  res.header("Access-Control-Allow-Origin","*");
  res.header("Access-Control-Allow-Headers","Origin, X-Requested-With, Content-Type, Accept");
  next();
})

app.use(bodyParser.urlencoded({
  extended: true
}))
app.use(bodyParser.json())

router.use((req, res, next) => {
  console.log('API called')
  next()
})

router.get('/', serviceGet.init);
router.route('/todos')
.get(serviceGet.getTodos);

router.route('/create').post(servicePost);

router.route('/update/:id').post(serviceUpdate.updateTodo)

router.route('/update/todo/:id').post(serviceUpdate.updateDoneTodo)

router.route('/delete/:id').post(serviceDelete.deleteTodo);

app.use(`${API_CONFIG.path}${API_CONFIG.version}`, router)

app.listen(PORT)
console.log(`running on : ${API_CONFIG.host}:${PORT}`)




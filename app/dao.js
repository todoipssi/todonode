const Todo=require('./models/todo');


class DAO {
    constructor(){
    
    }
    find(){
        return new Promise((resolve,reject)=>{
            Todo.find((err,todos)=>{
                if(err) {
                    this.log(`FIND ERROR ${err}`);
                    reject(err);
                }
                
                resolve(todos)
            });
        });
    }
    findOne(id){
        return new Promise((resolve,reject)=>{
            Todo.findById(id, (err,todos)=>{
                if(err) reject(err);
        
                resolve(todos);
            })
        });
    }
}
module.exports=new DAO();
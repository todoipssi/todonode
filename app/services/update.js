const Todo=require('../models/todo');

const DAO =require('../dao');
let updateTodo=(req,res)=>{
    Todo.findById(req.params.id,function(err,todo){
        if(err) res.send(err);

        todo.libelle=req.body.libelle;
        todo.updatedAt=new Date();
        todo.done=todo.done;
        todo.save((err,result)=>{
            if(err)
                res.send(err);
            res.json({
                id:result._id,
                libelle:result.libelle,
                done:result.done,
                updatedAt:result.updatedAt
            });
        });
    });
}
let updateDoneTodo=(req,res)=>{
    Todo.findById(req.params.id,function(err,todo){
        if(err) res.send(err);

        todo.libelle=todo.libelle;
        todo.updatedAt=new Date();
        if(todo.done==false)
        {
            todo.done=true;
        }
        else{
            todo.done=false;
        }
        todo.save((err,result)=>{
            if(err)
                res.send(err);
            res.json({
                id:result._id,
                libelle:result.libelle,
                done:result.done,
                updatedAt:result.updatedAt
            });
        });
    });
}


module.exports={
    updateTodo:updateTodo,
    updateDoneTodo:updateDoneTodo
};
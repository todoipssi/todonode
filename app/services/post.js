
const Todo=require('../models/todo');
function post(req,res){
    /*if(!check.checkProperties(req.body))
        return res.sendStatus(400);*/
    const todo=new Todo();

    todo.libelle=req.body.libelle;
    todo.done=false;
    todo.createdAt=new Date();

    todo.save((err,result)=>{
        if(err) res.send(err);

        res.json({message:'todos created',"libelle":result.libelle,"done":result.done});
    });
}module.exports=post;
const Todo=require('../models/todo');

const DAO =require('../dao');
let initMessage=()=>{
    return{
        message:'test todoNode'
    }
}
let init=(req,res)=>{
    const message=initMessage();
    res.json(message)
}

async function getTodos(req,res){
    try{
        const findTodos=await DAO.find();
        console.log('test', findTodos);
        res.json(findTodos);
    }
    catch(err){
        res.send(err);
    }
}


module.exports={
    init: init,
    initMessage:    initMessage,
    getTodos:getTodos
};